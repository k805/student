FROM tomcat
ADD https://github.com/devops2k18/DevOpsAug/raw/master/APPSTACK/student.war webapps/student.war
COPY context.xml conf/context.xml
ADD https://github.com/devops2k18/DevOpsAug/raw/master/APPSTACK/mysql-connector-java-5.1.40.jar lib/mysql-connector-java-5.1.40.jar
